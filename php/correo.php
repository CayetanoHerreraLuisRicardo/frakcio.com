<?php

$destino = 'contacto@frakcio.com';
$eol = PHP_EOL;
$asunto = 'Contacto frakcio';
$nombre = $_POST['name'];
$mail = $_POST['email'];
$telefono = $_POST['phone'];
$ciudad = $_POST['city'];
$mensaje = "";

$headers = "From: " . strip_tags($mail) . "\r\n";
$headers .= "Reply-To: " . strip_tags($mail) . "\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: text/html; charset=UTF-8\r\n";

if ($telefono != '') {
    $mensaje .= "<b>Teléfono:</b> " . $telefono;
}
if ($ciudad != '') {
    $mensaje .= "<br> <b>Ciudad:</b> " . $ciudad;
}
if ($mensaje != '') {
    $mensaje .= " <br> <b>Mensaje:</b> " . $_POST['message'];
}

if ($nombre != '' && $mail != '') {

    if (mail($destino, $asunto, $mensaje, $headers)) {

        echo "<script type='text/javascript'>alert('Muchas gracias por tu interés en Frakcio. Unos de nuestros colaboradores te contactará a la brevedad')</script>";
        echo "<script type='text/javascript'>window.location='http://www.frakcio.com/#cotiza'</script>";
    }
} else {
    echo "<script type='text/javascript'>alert('Favor de llenar todos los campos')</script>";
    echo "<script type='text/javascript'>window.location='http://www.frakcio.com/#cotiza'</script>";

}
